<?php

$con=mysqli_connect('localhost','root','','employee');
$sql="SELECT * FROM data";
$result= mysqli_query($con,$sql);
?>




<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    
  </head>
  <body>
    
    <div class="container">
      <div class="row clearfix">
        <div class="col-md-2">
          <br>
          <a href="add.php" class="btn btn-success">Insert</a> 
          <a href="search.php" class="btn btn-primary">Search</a>
          <a href="deleteall.php" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">delete all</a>
        </div>
        <div class="col-md-8">
          <p>Employee List</p>
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <?php
            while ($row= mysqli_fetch_assoc($result)) {


          ?>
           <tr>
              <th><?php echo $row['id']?></th>
              <th><?php echo $row['name']?></th>
              <th><?php echo $row['email']?></th>
              <th> 
                <a class="btn btn-info btn-sm" href="view.php?id=<?php echo $row['id']?>">View</a>
                <a  class="btn btn-primary btn-sm" href="edit.php?id=<?php echo $row['id']?>">edit</a>
                <a href="delete.php?id=<?php echo $row['id']?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">delete</a>
              </th>
            </tr>
          <?php }
          ?>            
          </table>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>